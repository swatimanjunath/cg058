//write your code here
#include<stdio.h>
int input()
{
	int a;
	printf("Enter the number to find multiples");
	scanf("%d",&a);
	return a;
}
int compute(int j,int b)
{
	if (j%b==0)
		return j;
	else 
		return 0;
}
void display(int k,int c)
{
	if (k>0) 
		printf("\n%d is a multiple of %d",k,c);
}
int main()
{
	int p,i;
	p=input();
	for(i=1;i<=100;i++)
	{
		int q=compute(i,p);
		display(q,p);
	}
	return 0;
}