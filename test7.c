//write your code here
#include<stdio.h>
int input()
{
    int a;
    scanf("%d",&a);
    return a;
}
void display(int a[][10],int c,int b)
{
    for(int i=0;i<c;i++)
    {
        for(int j=0;j<b;j++)
        {
            printf("%d\t",a[i][j]);
        }
    
        printf("\n");
    }
}
int main()
{
    
    int matrix[10][10], transpose[10][10];
    int a,b;
    printf("Enter the number of rows and columns\n");
    scanf("%d%d",&a,&b);
    for(int i=0;i<a;i++)
    {
        printf("Enter the elememts of the %dth row\n",i);
        for(int j=0;j<b;j++)
        {
            matrix[i][j]=input();
        }
    }
     for(int i=0;i<a;i++)
    {
        for(int j=0;j<b;j++)
        {
            transpose[j][i]=matrix[i][j];
        }
    }
    display(transpose,b,a);
     
    return 0;
}