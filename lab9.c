//write your code here
#include<stdio.h>
int input()
{
    int a;
    scanf("%d",&a);
    return a;
}
void swap( int *var1, int *var2)
{
    int temp;
    temp = *var2;
    *var2 = *var1;
    *var1 = temp;
}
int main()
{
    int num1, num2;
    printf("Enter the values of the two numbers\n");
    num1=input();
    num2=input();
    printf("Before swapping the numbers are \n");
    printf("Number 1: %d",num1);
    printf("\nNumber 2: %d",num2);
    
    swap(&num1, &num2);
    
    printf("\nAfter swapping the numbers are \n");
    printf("Number 1: %d\n",num1);
    printf("Number 2: %d",num2);
    return 0;
}