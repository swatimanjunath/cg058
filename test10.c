//write your code here
#include<stdio.h>
int input()
{
    int a;
    scanf("%d",&a);
    return a;
}
void add( int *var1, int *var2, int *d)
{
    *d=(*var1)+(*var2);
}
void sub( int *var1, int *var2, int *d)
{
    *d=(*var1)-(*var2);
}
void mul( int *var1, int *var2, float *d)
{
    *d=(*var1)*(*var2);
}
void divi( int *var1, int *var2, float *d)
{
    *d=(*var1)/(*var2);
}
void rem( int *var1, int *var2, float *d)
{
    *d=(*var1)%(*var2);
}
int main()
{
    int num1, num2,a,s;
    float di, mu,re;
    printf("Enter the values of the two numbers\n");
    num1=input();
    num2=input();
    
    add(&num1, &num2 , &a);
    sub(&num1, &num2 , &s);
    mul(&num1, &num2 , &mu);
    divi(&num1, &num2 , &di);
    rem(&num1, &num2 , &re);
    printf("\nThe sum of the two numbers is %d",a);
    printf("\nThe difference of the two numbers is %d",s);
    printf("\nThe product of the two numbers is %f",mu);
    printf("\nThe quotient of the two numbers is %f",di);
    printf("\nThe remainder of the two numbers is %f",re);
    return 0;
}