#include<stdio.h>
#include<math.h>
int main()
{
    int n;
    float sum=0.0;
    printf("Enter the value of n\n");
    scanf("%d",&n);
    for(int i=1;i<=n;i++)
    {
        float t=1/(pow(i,2));
        sum=sum+t;
    }
    printf("Sum of series upto %d is %f",n,sum);
    return 0;
}